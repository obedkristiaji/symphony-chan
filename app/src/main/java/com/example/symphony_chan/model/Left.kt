package com.example.symphony_chan.model

class Left(private var title: String, private var id: Int) {

    fun getId(): Int {
        return this.id
    }

    fun getTitle(): String {
        return this.title
    }
}