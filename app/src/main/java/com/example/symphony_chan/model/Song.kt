package com.example.symphony_chan.model

class Song(private var id: String, private var albumId: String, private var image: String, private var title: String, private var artist: String, private var release: String) {

    fun getId(): String {
        return this.id
    }

    fun getAlbumId(): String {
        return this.albumId
    }

    fun getImage(): String {
        return this.image
    }

    fun getTitle(): String {
        return this.title
    }

    fun getArtist(): String {
        return this.artist
    }

    fun getRelease(): String {
        return this.release
    }

    fun setImage(url: String) {
        this.image = url
    }
}