package com.example.symphony_chan.model

class Artist(private var id: String, private var name: String, private var detail: String, private var country: String, private var birth: String) {

    fun getId(): String {
        return this.id
    }

    fun getName(): String {
        return this.name
    }

    fun getDetail(): String {
        return this.detail
    }

    fun getCountry(): String {
        return this.country
    }

    fun getBirth(): String {
        return this.birth
    }
}