package com.example.symphony_chan.storage

import android.content.Context
import android.content.SharedPreferences
import com.example.symphony_chan.model.Artist
import com.example.symphony_chan.model.Song
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class DataStorage(context: Context) {
    protected var sharedPref: SharedPreferences
    companion object {
        protected var NAMA_SHARED_PREF: String = "sp_data"
        protected var KEY_ARTIST: String = "LIST_ARTIST"
        protected var KEY_SONG: String = "LIST_SONG"
        protected var KEY_DARK: String = "DARK_MODE"
    }

    init {
        this.sharedPref = context.getSharedPreferences(NAMA_SHARED_PREF, 0)
    }

    fun clearHistory() {
        this.sharedPref.edit().putString(KEY_ARTIST, "[]").commit()
        this.sharedPref.edit().putString(KEY_SONG, "[]").commit()
    }

    fun saveArtist(listArtist: List<Artist>) {
        val artist = Gson().toJson(listArtist)
        val editor: SharedPreferences.Editor = this.sharedPref.edit()
        editor.putString(KEY_ARTIST, artist)
        editor.commit()
    }

    fun saveSong(listSong: List<Song>) {
        val song = Gson().toJson(listSong)
        val editor: SharedPreferences.Editor = this.sharedPref.edit()
        editor.putString(KEY_SONG, song)
        editor.commit()
    }

    fun getArtist(): List<Artist> {
        val artist = this.sharedPref.getString(KEY_ARTIST, "")
        val token = object : TypeToken<List<Artist>>() { }.type

        return Gson().fromJson(artist, token) ?: listOf()
    }

    fun getSong(): List<Song> {
        val song = this.sharedPref.getString(KEY_SONG, "")
        val token = object : TypeToken<List<Song>>() { }.type

        return Gson().fromJson(song, token) ?: listOf()
    }

    fun saveDark(dark: Boolean) {
        val editor: SharedPreferences.Editor = this.sharedPref.edit()
        editor.putBoolean(KEY_DARK, dark)
        editor.commit()
    }

    fun getDark(): Boolean {
        return sharedPref.getBoolean(KEY_DARK, false)
    }
}