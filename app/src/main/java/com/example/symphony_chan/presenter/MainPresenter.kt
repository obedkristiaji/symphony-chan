package com.example.symphony_chan.presenter

import android.content.Context
import android.widget.*
import androidx.appcompat.app.AppCompatDelegate
import com.example.symphony_chan.R
import com.example.symphony_chan.model.Album
import com.example.symphony_chan.model.Artist
import com.example.symphony_chan.model.Song
import com.example.symphony_chan.storage.DataStorage
import com.example.symphony_chan.task.*
import com.example.symphony_chan.view.MainActivity

class MainPresenter(private val view: MainActivity): IMainPresenter {
    private val db: DataStorage = DataStorage(this.view)
    private var songList: MutableList<Song> = mutableListOf()
    private var albumList: MutableList<Album> = mutableListOf()
    private var artistList: MutableList<Artist> = mutableListOf()
    private var songStorage: MutableList<Song> = mutableListOf()
    private var artistStorage: MutableList<Artist> = mutableListOf()
    private var historyList: MutableList<Song> = mutableListOf()
    private var artist: Artist = Artist("","","","","")
    private var album: Album = Album("","","","","")
    private var song: Song = Song("","","","","", "")
    private var dark: Boolean
    private var text: String = ""
    private var spinnerId: Long = 0

    init {
        if(!this.db.getArtist().isEmpty()) {
            this.artistStorage = this.db.getArtist() as MutableList<Artist>
        }
        if(!this.db.getSong().isEmpty()) {
            this.songStorage = this.db.getSong() as MutableList<Song>
        }
        this.dark = this.db.getDark()
    }


    override fun addSong(song: Song) {
        this.songList.add(song)
    }

    override fun addAlbum(album: Album) {
        this.albumList.add(album)
    }

    override fun addArtist(artist: Artist) {
        this.artistList.add(artist)
    }

    override fun getSongList(): MutableList<Song> {
        return this.songList
    }

    override fun getAlbumList(): MutableList<Album> {
        return this.albumList
    }

    override fun getArtistList(): MutableList<Artist> {
        return this.artistList
    }

    override fun emptyList() {
        this.songList = mutableListOf<Song>()
        this.artistList = mutableListOf<Artist>()
    }

    override fun closeKey() {
        this.view.closeKeyboard(false)
    }

    override fun loadArtist(artist: Artist, context: Context) {
        this.artist = artist
        this.addArtistStorage(artist)
        this.view.changePage("Artist")
        this.loadAlbumList(context)
    }

    override fun loadAlbum(album: Album, context: Context) {
        this.album = album
        val task = LookupAlbumCoverTask(this, view, context)
        if(this.view.checkNetworkConn()) {
            this.view.setVisible()
            task.execute(album)
        }
        else {
            Toast.makeText(context, "Tidak Ada Koneksi Internet", Toast.LENGTH_SHORT).show()
        }
    }

    override fun loadSong(song: Song, context: Context) {
        this.song = song
        val task = LookupSongCoverTask(this, view, context)
        if(this.view.checkNetworkConn()) {
            this.view.setVisible()
            task.execute(song)
        }
        else {
            Toast.makeText(context, "Tidak Ada Koneksi Internet", Toast.LENGTH_SHORT).show()
        }
    }

    override fun getArtist(): Artist {
        return this.artist
    }

    override fun getAlbum(): Album {
        return this.album
    }

    override fun getSong(): Song {
        return this.song
    }

    override fun loadSearchList(etSearch: EditText, spinnerSearch: Spinner, context: Context) {
        val task = SearchTask(this, view, context)
        val query = etSearch.text.toString()
        var category = if (spinnerSearch.getSelectedItem().toString()=="Artist") "artist" else "recording"

        if(this.view.checkNetworkConn()) {
            if(query.isEmpty()) {
                Toast.makeText(context, "Input Kosong", Toast.LENGTH_SHORT).show()
            }
            else {
                this.view.setVisible()
                task.execute(query, category)
            }
        }
        else {
            Toast.makeText(context, "Tidak Ada Koneksi Internet", Toast.LENGTH_SHORT).show()
        }
    }

    override fun loadAlbumList(context: Context) {
        this.albumList = mutableListOf<Album>()
        val task = LookupAlbumTask(this, view, context)

        if(this.view.checkNetworkConn()) {
            this.view.setVisible()
            task.execute(artist)
        }
        else {
            Toast.makeText(context, "Tidak Ada Koneksi Internet", Toast.LENGTH_SHORT).show()
        }
    }

    override fun loadSongList(context: Context) {
        this.songList = mutableListOf<Song>()
        val task = LookupSongTask(this, view, context)

        if(this.view.checkNetworkConn()) {
            this.view.setVisible()
            task.execute(album)
        }
        else {
            Toast.makeText(context, "Tidak Ada Koneksi Internet", Toast.LENGTH_SHORT).show()
        }
    }

    override fun addSongStorage(song: Song) {
        var same = false

        for(item in songStorage) {
            if(item.getId() == song.getId()) {
                same = true
                break
            }
        }

        if(!same) {
            this.songStorage.add(song)
            this.db.saveSong(this.songStorage)
        }
    }

    override fun addArtistStorage(artist: Artist) {
        var same = false

        for(item in artistStorage) {
            if(item.getId() == artist.getId()) {
                same = true
                break
            }
        }

        if(!same) {
            this.artistStorage.add(artist)
            this.db.saveArtist(this.artistStorage)
        }
    }

    override fun getSongStorage(): List<Song> {
        return this.db.getSong()
    }

    override fun getArtistStorage(): List<Artist> {
        return this.db.getArtist()
    }

    override fun clearData() {
        this.db.clearHistory()
        this.artistStorage = this.db.getArtist() as MutableList<Artist>
        this.songStorage = this.db.getSong() as MutableList<Song>
    }

    override fun darkEdit(et: EditText) {
        if(this.dark) {
            et.setBackgroundResource(R.drawable.border_dark)
        } else {
            et.setBackgroundResource(R.drawable.border_light)
        }
    }

    override fun darkSpinner(spinner: Spinner) {
        if(this.dark) {
            spinner.setBackgroundResource(R.drawable.border_dark)
        } else {
            spinner.setBackgroundResource(R.drawable.border_light)
        }
    }

    override fun darkBtn(btn: Button) {
        if(this.dark) {
            btn.setText(R.string.setting_btn_light)
        } else {
            btn.setText(R.string.setting_btn_dark)
        }
    }

    override fun darkMode() {
        this.dark = !this.dark
        this.db.saveDark(this.dark)
        if(this.dark) {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
        } else {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
        }
        this.view.reloadActivity()
    }

    override fun setText(text: String, spinnerId: Long) {
        this.text = text
        this.spinnerId = spinnerId
    }

    override fun getText(): String {
        return this.text
    }

    override fun getSpinnerId(): Long {
        return this.spinnerId
    }

    override fun deleteSongStorage(position: Int) {
        this.songStorage.removeAt(position)
        this.db.saveSong(this.songStorage)
    }

    override fun deleteArtistStorage(position: Int) {
        this.artistStorage.removeAt(position)
        this.db.saveArtist(this.artistStorage)
    }

    override fun reload(tag: String) {
        this.view.reloadFragment(tag)
    }

    override fun getHistoryList(): MutableList<Song> {
        return this.historyList
    }

    override fun setHistoryList(songList: MutableList<Song>) {
        this.historyList = songList
    }
}