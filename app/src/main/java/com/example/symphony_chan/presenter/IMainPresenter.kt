package com.example.symphony_chan.presenter

import android.content.Context
import android.widget.*
import com.example.symphony_chan.model.Album
import com.example.symphony_chan.model.Artist
import com.example.symphony_chan.model.Song

interface IMainPresenter {
    fun addSong(song: Song)
    fun addAlbum(album: Album)
    fun addArtist(artist: Artist)
    fun getSongList(): MutableList<Song>
    fun getAlbumList(): MutableList<Album>
    fun getArtistList(): MutableList<Artist>
    fun emptyList()
    fun closeKey()
    fun loadArtist(artist: Artist, context: Context)
    fun loadAlbum(album: Album, context: Context)
    fun loadSong(song: Song, context: Context)
    fun getArtist(): Artist
    fun getAlbum(): Album
    fun getSong(): Song
    fun loadSearchList(etSearch: EditText, spinnerSearch: Spinner, context: Context)
    fun loadAlbumList(context: Context)
    fun loadSongList(context: Context)
    fun addSongStorage(song: Song)
    fun addArtistStorage(artist: Artist)
    fun getSongStorage(): List<Song>
    fun getArtistStorage(): List<Artist>
    fun clearData()
    fun darkEdit(et: EditText)
    fun darkSpinner(spinner: Spinner)
    fun darkBtn(btn: Button)
    fun darkMode()
    fun setText(text: String, spinnerId: Long)
    fun getText(): String
    fun getSpinnerId(): Long
    fun deleteSongStorage(position: Int)
    fun deleteArtistStorage(position: Int)
    fun reload(tag: String)
    fun getHistoryList(): MutableList<Song>
    fun setHistoryList(songList: MutableList<Song>)
}