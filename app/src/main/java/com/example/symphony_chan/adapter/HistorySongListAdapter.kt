package com.example.symphony_chan.adapter

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.symphony_chan.databinding.HistorySongListBinding
import com.example.symphony_chan.model.Song
import com.example.symphony_chan.presenter.IMainPresenter
import com.h6ah4i.android.widget.advrecyclerview.swipeable.SwipeableItemAdapter
import com.h6ah4i.android.widget.advrecyclerview.swipeable.SwipeableItemConstants
import com.h6ah4i.android.widget.advrecyclerview.swipeable.action.SwipeResultAction
import com.h6ah4i.android.widget.advrecyclerview.swipeable.action.SwipeResultActionDoNothing
import com.h6ah4i.android.widget.advrecyclerview.swipeable.action.SwipeResultActionRemoveItem
import com.h6ah4i.android.widget.advrecyclerview.utils.AbstractSwipeableItemViewHolder

class HistorySongListAdapter(context: Context, presenter: IMainPresenter): RecyclerView.Adapter<HistorySongListAdapter.ViewHolder>(), SwipeableItemAdapter<HistorySongListAdapter.ViewHolder> {
    private var view: Context = context
    private var songList: List<Song> = ArrayList()
    private var presenter = presenter

    init {
        setHasStableIds(true)
    }

    fun update(song: List<Song>) {
        this.songList = song
        this.notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView: View
        val viewHolder: ViewHolder

        itemView = HistorySongListBinding.inflate(LayoutInflater.from(this.view)).root
        viewHolder = ViewHolder(itemView, presenter)
        itemView.tag = viewHolder
        return ViewHolder(itemView, presenter)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val song: Song = songList.get(position)
        holder.binding.tvSongTitle.text = song.getTitle()
        holder.binding.tvArtistName.text = song.getArtist()


        holder.binding.songList.setOnClickListener {
            presenter.loadSong(song, view)
        }
    }

    override fun getItemCount(): Int {
        return this.songList.size
    }

    override fun getItemId(position: Int): Long = position.toLong()

    class ViewHolder(view: View, presenter: IMainPresenter): AbstractSwipeableItemViewHolder(view) {
        val binding: HistorySongListBinding = HistorySongListBinding.bind(view)
        val presenter: IMainPresenter = presenter
        val OPTIONS_AREA_PROPORTION = 0.2f
        val REMOVE_ITEM_THRESHOLD = 0.6f
        var lastSwipeAmount = 0f

        override fun getSwipeableContainerView(): View {
            return this.binding.container
        }

        override fun onSlideAmountUpdated(
                horizontalAmount: Float,
                verticalAmount: Float,
                isSwiping: Boolean
        ) {
            val itemWidth = itemView.width
            val optionItemWidth: Float = itemWidth * OPTIONS_AREA_PROPORTION
            val offset = (optionItemWidth + 0.5f).toInt()
            val p: Float = Math.max(0f, Math.min(OPTIONS_AREA_PROPORTION, -horizontalAmount)) / OPTIONS_AREA_PROPORTION

            if (this.binding.optionView.width === 0) {
                setLayoutWidth(this.binding.optionView, (optionItemWidth + 0.5f).toInt())
            }

            this.binding.optionView.setTranslationX((-(p * optionItemWidth + 0.5f)) + offset)
            this.binding.btnDelete

            if (horizontalAmount < -REMOVE_ITEM_THRESHOLD) {
                this.binding.container.setVisibility(View.INVISIBLE)
                this.binding.optionView.setVisibility(View.INVISIBLE)
                this.binding.btnDelete.setVisibility(View.INVISIBLE)
            } else {
                this.binding.container.setVisibility(View.VISIBLE)
                this.binding.optionView.setVisibility(View.VISIBLE)
                this.binding.btnDelete.setVisibility(View.VISIBLE)
            }

            lastSwipeAmount = horizontalAmount
        }

        private fun setLayoutWidth(v: View, width: Int) {
            val lp = v.layoutParams as ViewGroup.LayoutParams
            lp.width = width
            v.setLayoutParams(lp)
        }
    }

    override fun onGetSwipeReactionType(holder: ViewHolder, position: Int, x: Int, y: Int): Int {
        return SwipeableItemConstants.REACTION_CAN_SWIPE_LEFT
    }

    override fun onSwipeItemStarted(holder: ViewHolder, position: Int) {
        this.notifyDataSetChanged()
    }

    override fun onSetSwipeBackground(holder: ViewHolder, position: Int, type: Int) {
        if (type == SwipeableItemConstants.DRAWABLE_SWIPE_LEFT_BACKGROUND) {
            holder.itemView.setBackgroundColor(Color.TRANSPARENT)
        }
    }

    override fun onSwipeItem(holder: ViewHolder, position: Int, result: Int): SwipeResultAction? {
        return if (result === SwipeableItemConstants.RESULT_SWIPED_LEFT) {
            return SwipeLeftRemoveAction(this, position, this.presenter)
        } else {
            SwipeResultActionDoNothing()
        }
    }

    class SwipeLeftRemoveAction(val adapter: HistorySongListAdapter, val position: Int, val presenter: IMainPresenter) : SwipeResultActionRemoveItem() {
        override fun onPerformAction() {
            presenter.deleteSongStorage(position)
            adapter.notifyItemRemoved(position)
            presenter.reload("History")
        }
    }
}