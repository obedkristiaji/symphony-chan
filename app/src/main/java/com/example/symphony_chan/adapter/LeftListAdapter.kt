package com.example.symphony_chan.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import com.example.symphony_chan.databinding.LeftListBinding
import com.example.symphony_chan.model.Left
import com.example.symphony_chan.view.IMainActivity

class LeftListAdapter(context: Context, data: List<Left>, private val listener: IMainActivity): ArrayAdapter<Left>(context, 0, data) {
    private var leftList: List<Left> = data
    private var view: Context = context

    override fun getItem(position: Int): Left {
        return leftList[position]
    }

    override fun getView(position : Int, view : View?, parent : ViewGroup) : View {
        val itemView: View
        val viewHolder: ViewHolder

        if (view == null) {
            itemView = LeftListBinding.inflate(LayoutInflater.from(this.view)).root
            viewHolder = ViewHolder(itemView, listener)
            itemView.tag = viewHolder
        } else {
            itemView = view
            viewHolder = view.tag as ViewHolder
        }

        viewHolder.updateView(this.getItem(position))
        return itemView
    }

    private class ViewHolder(view: View, listener: IMainActivity) {
        private val binding: LeftListBinding = LeftListBinding.bind(view)
        private val listener: IMainActivity = listener

        fun updateView(left: Left) {
            this.binding.tvLeft.text = left.getTitle()

            this.binding.tvLeft.setOnClickListener {
                when(left.getId()) {
                    1 -> this.listener.changePage("Home")
                    2 -> this.listener.changePage("Search")
                    3 -> this.listener.changePage("History")
                    4 -> this.listener.changePage("Setting")
                    5 -> this.listener.closeApplication()
                }
            }
        }
    }
}