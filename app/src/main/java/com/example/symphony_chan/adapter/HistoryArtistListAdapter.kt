package com.example.symphony_chan.adapter

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.symphony_chan.databinding.HistoryArtistListBinding
import com.example.symphony_chan.model.Artist
import com.example.symphony_chan.presenter.IMainPresenter
import com.h6ah4i.android.widget.advrecyclerview.swipeable.SwipeableItemAdapter
import com.h6ah4i.android.widget.advrecyclerview.swipeable.SwipeableItemConstants
import com.h6ah4i.android.widget.advrecyclerview.swipeable.action.SwipeResultAction
import com.h6ah4i.android.widget.advrecyclerview.swipeable.action.SwipeResultActionDoNothing
import com.h6ah4i.android.widget.advrecyclerview.swipeable.action.SwipeResultActionRemoveItem
import com.h6ah4i.android.widget.advrecyclerview.utils.AbstractSwipeableItemViewHolder

class HistoryArtistListAdapter(context: Context, presenter: IMainPresenter): RecyclerView.Adapter<HistoryArtistListAdapter.ViewHolder>(), SwipeableItemAdapter<HistoryArtistListAdapter.ViewHolder> {
    private var view: Context = context
    private var artistList: List<Artist> = ArrayList()
    private var presenter = presenter

    init {
        setHasStableIds(true)
    }

    fun update(artist: List<Artist>) {
        this.artistList = artist
        this.notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView: View
        val viewHolder: ViewHolder

        itemView = HistoryArtistListBinding.inflate(LayoutInflater.from(this.view)).root
        viewHolder = ViewHolder(itemView, presenter)
        itemView.tag = viewHolder
        return ViewHolder(itemView, presenter)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val artist: Artist = artistList.get(position)
        holder.binding.tvArtistCountry.text = artist.getCountry()
        holder.binding.tvArtistName.text = artist.getName()


        holder.binding.artistList.setOnClickListener {
            presenter.loadArtist(artist, view)
        }
    }

    override fun getItemCount(): Int {
        return this.artistList.size
    }

    override fun getItemId(position: Int): Long = position.toLong()

    class ViewHolder(view: View, presenter: IMainPresenter): AbstractSwipeableItemViewHolder(view) {
        val binding: HistoryArtistListBinding = HistoryArtistListBinding.bind(view)
        val presenter: IMainPresenter = presenter
        val OPTIONS_AREA_PROPORTION = 0.2f
        val REMOVE_ITEM_THRESHOLD = 0.6f
        var lastSwipeAmount = 0f

        override fun getSwipeableContainerView(): View {
            return this.binding.container
        }

        override fun onSlideAmountUpdated(
                horizontalAmount: Float,
                verticalAmount: Float,
                isSwiping: Boolean
        ) {
            val itemWidth = itemView.width
            val optionItemWidth: Float = itemWidth * OPTIONS_AREA_PROPORTION
            val offset = (optionItemWidth + 0.5f).toInt()
            val p: Float = Math.max(0f, Math.min(OPTIONS_AREA_PROPORTION, -horizontalAmount)) / OPTIONS_AREA_PROPORTION

            if (this.binding.optionView.width === 0) {
                setLayoutWidth(this.binding.optionView, (optionItemWidth + 0.5f).toInt())
            }

            this.binding.optionView.setTranslationX((-(p * optionItemWidth + 0.5f)) + offset)
            this.binding.btnDelete

            if (horizontalAmount < -REMOVE_ITEM_THRESHOLD) {
                this.binding.container.setVisibility(View.INVISIBLE)
                this.binding.optionView.setVisibility(View.INVISIBLE)
                this.binding.btnDelete.setVisibility(View.INVISIBLE)
            } else {
                this.binding.container.setVisibility(View.VISIBLE)
                this.binding.optionView.setVisibility(View.VISIBLE)
                this.binding.btnDelete.setVisibility(View.VISIBLE)
            }

            lastSwipeAmount = horizontalAmount
        }

        private fun setLayoutWidth(v: View, width: Int) {
            val lp = v.layoutParams as ViewGroup.LayoutParams
            lp.width = width
            v.setLayoutParams(lp)
        }
    }

    override fun onGetSwipeReactionType(holder: ViewHolder, position: Int, x: Int, y: Int): Int {
        return SwipeableItemConstants.REACTION_CAN_SWIPE_LEFT
    }

    override fun onSwipeItemStarted(holder: ViewHolder, position: Int) {
        this.notifyDataSetChanged()
    }

    override fun onSetSwipeBackground(holder: ViewHolder, position: Int, type: Int) {
        if (type == SwipeableItemConstants.DRAWABLE_SWIPE_LEFT_BACKGROUND) {
            holder.itemView.setBackgroundColor(Color.TRANSPARENT)
        }
    }

    override fun onSwipeItem(holder: ViewHolder, position: Int, result: Int): SwipeResultAction? {
        return if (result === SwipeableItemConstants.RESULT_SWIPED_LEFT) {
            return SwipeLeftRemoveAction(this, position, this.presenter)
        } else {
            SwipeResultActionDoNothing()
        }
    }

    class SwipeLeftRemoveAction(val adapter: HistoryArtistListAdapter, val position: Int, val presenter: IMainPresenter) : SwipeResultActionRemoveItem() {
        override fun onPerformAction() {
            presenter.deleteArtistStorage(position)
            adapter.notifyItemRemoved(position)
            presenter.reload("History")
        }
    }
}