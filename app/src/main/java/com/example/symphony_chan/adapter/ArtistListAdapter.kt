package com.example.symphony_chan.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.example.symphony_chan.databinding.ArtistListBinding
import com.example.symphony_chan.model.Artist
import com.example.symphony_chan.presenter.IMainPresenter

class ArtistListAdapter(context: Context, presenter: IMainPresenter): BaseAdapter() {
    private var view: Context = context
    private var artistList: List<Artist> = ArrayList()
    private var presenter = presenter

    fun update(artist: List<Artist>) {
        this.artistList = artist
        this.notifyDataSetChanged()
    }

    override fun getCount(): Int {
        return artistList.size
    }

    override fun getItem(position: Int): Artist {
        return artistList[position]
    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    override fun getView(position : Int, view : View?, parent : ViewGroup) : View {
        val itemView: View
        val viewHolder: ViewHolder

        if (view == null) {
            itemView = ArtistListBinding.inflate(LayoutInflater.from(this.view)).root
            viewHolder = ViewHolder(itemView, presenter)
            itemView.tag = viewHolder
        } else {
            itemView = view
            viewHolder = view.tag as ViewHolder
        }

        viewHolder.updateView(this.getItem(position), this.view)
        return itemView
    }

    private class ViewHolder(view: View, presenter: IMainPresenter) {
        private val binding: ArtistListBinding = ArtistListBinding.bind(view)
        private val presenter: IMainPresenter = presenter

        fun updateView(artist: Artist, context: Context) {
            this.binding.tvArtistName.text = artist.getName()
            this.binding.tvArtistCountry.text = artist.getCountry()

            this.binding.artistList.setOnClickListener {
                presenter.loadArtist(artist, context)
            }
        }
    }
}