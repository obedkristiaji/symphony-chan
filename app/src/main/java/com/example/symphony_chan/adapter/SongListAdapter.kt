package com.example.symphony_chan.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.example.symphony_chan.databinding.SongListBinding
import com.example.symphony_chan.model.Song
import com.example.symphony_chan.presenter.IMainPresenter

class SongListAdapter(context: Context, presenter: IMainPresenter): BaseAdapter() {
    private var view: Context = context
    private var songList: List<Song> = ArrayList()
    private var presenter = presenter

    fun update(song: List<Song>) {
        this.songList = song
        this.notifyDataSetChanged()
    }

    override fun getCount(): Int {
        return songList.size
    }

    override fun getItem(position: Int): Song {
        return songList[position]
    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    override fun getView(position : Int, view : View?, parent : ViewGroup) : View {
        val itemView: View
        val viewHolder: ViewHolder

        if (view == null) {
            itemView = SongListBinding.inflate(LayoutInflater.from(this.view)).root
            viewHolder = ViewHolder(itemView, presenter)
            itemView.tag = viewHolder
        } else {
            itemView = view
            viewHolder = view.tag as ViewHolder
        }

        viewHolder.updateView(this.getItem(position), this.view)
        return itemView
    }

    private class ViewHolder(view: View, presenter: IMainPresenter) {
        private val binding: SongListBinding = SongListBinding.bind(view)
        private val presenter: IMainPresenter = presenter

        fun updateView(song: Song, context: Context) {
            this.binding.tvSongTitle.text = song.getTitle()
            this.binding.tvArtistName.text = song.getArtist()

            this.binding.songList.setOnClickListener {
                presenter.loadSong(song, context)
            }
        }
    }
}