package com.example.symphony_chan.view

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.symphony_chan.adapter.HistoryArtistListAdapter
import com.example.symphony_chan.adapter.HistorySongListAdapter
import com.example.symphony_chan.databinding.FragmentHistoryBinding
import com.example.symphony_chan.presenter.IMainPresenter
import com.h6ah4i.android.widget.advrecyclerview.animator.SwipeDismissItemAnimator
import com.h6ah4i.android.widget.advrecyclerview.swipeable.RecyclerViewSwipeManager

class HistoryFragment : Fragment(), AdapterView.OnItemSelectedListener {
    private lateinit var binding: FragmentHistoryBinding
    private lateinit var listener: IMainActivity
    private lateinit var presenter: IMainPresenter

    init {

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        this.binding = FragmentHistoryBinding.inflate(inflater, container, false)

        this.binding.spinnerHistory.setOnItemSelectedListener(this)

        return this.binding.root
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if(context is IMainActivity) {
            this.listener = context as IMainActivity
        } else {
            throw ClassCastException(
                context.toString()
                        + " must implement FragmentListener"
            )
        }
    }

    override fun onResume() {
        super.onResume()
        this.presenter.darkSpinner(this.binding.spinnerHistory)
    }

    companion object {
        fun newInstance(presenter: IMainPresenter): HistoryFragment {
            val fragment = HistoryFragment()
            fragment.presenter = presenter
            return fragment
        }
    }

    override fun onItemSelected(parent: AdapterView<*>, view: View?, position: Int, id: Long) {
        val item = parent.getItemAtPosition(position).toString()

        val lstSearch: RecyclerView = binding.lstHistory as RecyclerView
        val swipeManager = RecyclerViewSwipeManager()
        lstSearch.layoutManager = LinearLayoutManager(context)

        if(item == "Artist") {
            val artistAdapter = HistoryArtistListAdapter(activity!!, this.presenter)
            artistAdapter.update(this.presenter.getArtistStorage())
            val wrappedAdapter = swipeManager.createWrappedAdapter(artistAdapter) as RecyclerView.Adapter
            lstSearch.adapter = wrappedAdapter
            lstSearch.itemAnimator = SwipeDismissItemAnimator()
            swipeManager.attachRecyclerView(lstSearch)
        } else {
            val songAdapter = HistorySongListAdapter(activity!!, this.presenter)
            songAdapter.update(this.presenter.getSongStorage())
            val wrappedAdapter = swipeManager.createWrappedAdapter(songAdapter) as RecyclerView.Adapter
            lstSearch.adapter = wrappedAdapter
            lstSearch.itemAnimator = SwipeDismissItemAnimator()
            swipeManager.attachRecyclerView(lstSearch)
        }
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
        TODO("Not yet implemented")
    }
}
