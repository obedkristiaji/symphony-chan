package com.example.symphony_chan.view

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ListView
import androidx.fragment.app.Fragment
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.bumptech.glide.load.model.GlideUrl
import com.bumptech.glide.load.model.LazyHeaders
import com.example.symphony_chan.adapter.SongListAdapter
import com.example.symphony_chan.databinding.FragmentAlbumBinding
import com.example.symphony_chan.model.Album
import com.example.symphony_chan.presenter.IMainPresenter

class AlbumFragment : Fragment() {
    private lateinit var binding: FragmentAlbumBinding
    private lateinit var listener: IMainActivity
    private lateinit var presenter: IMainPresenter
    private lateinit var album: Album

    init {

    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        this.binding = FragmentAlbumBinding.inflate(inflater, container, false)

        this.album = this.presenter.getAlbum()

        val circularProgressDrawable = CircularProgressDrawable(context!!)
        circularProgressDrawable.strokeWidth = 5f
        circularProgressDrawable.centerRadius = 30f
        circularProgressDrawable.start()

        val glideUrl = GlideUrl(album.getImage(), LazyHeaders.Builder()
                .addHeader("Accept", "image/jpeg")
                .build()
        )

        GlideApp
            .with(this)
            .load(glideUrl)
            .placeholder(circularProgressDrawable)
            .into(this.binding.ivSong)

        this.initiateAlbum()

        return this.binding.root
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if(context is IMainActivity) {
            this.listener = context as IMainActivity
        } else {
            throw ClassCastException(context.toString()
                    + " must implement FragmentListener")
        }
    }

    override fun onResume() {
        super.onResume()
        this.initializeAdapter()
    }

    companion object {
        fun newInstance(presenter: IMainPresenter): AlbumFragment {
            val fragment = AlbumFragment()
            fragment.presenter = presenter
            return fragment
        }
    }

    private fun initiateAlbum() {
        this.binding.tvTitle.text = this.album.getTitle()
        this.binding.tvArtist.text = this.album.getArtist()
        this.binding.tvRelease.text = this.album.getRelease()
    }

    fun initializeAdapter() {
        val lstSong: ListView = binding.lstSong as ListView
        val songAdapter = SongListAdapter(activity!!, this.presenter)
        lstSong.adapter = songAdapter
        (lstSong.adapter as SongListAdapter).update(this.presenter.getSongList())
    }
}