package com.example.symphony_chan.view

import android.content.Context
import android.content.pm.ActivityInfo
import android.net.ConnectivityManager
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.FrameLayout
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.example.symphony_chan.R
import com.example.symphony_chan.databinding.ActivityMainBinding
import com.example.symphony_chan.presenter.MainPresenter

class MainActivity : AppCompatActivity(), IMainActivity {
    private lateinit var binding: ActivityMainBinding
    private lateinit var fragmentManager: FragmentManager
    private lateinit var fragmentHome: HomeFragment
    private lateinit var fragmentSearch: SearchFragment
    private lateinit var fragmentHistory: HistoryFragment
    private lateinit var fragmentArtist: ArtistFragment
    private lateinit var fragmentAlbum: AlbumFragment
    private lateinit var fragmentSong: SongFragment
    private lateinit var fragmentSetting: SettingFragment
    private lateinit var presenter: MainPresenter
    private lateinit var connMgr: ConnectivityManager
    private lateinit var progressOverlay: FrameLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT)
        super.onCreate(savedInstanceState)
        this.binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(this.binding.root)

        this.setSupportActionBar(binding.toolbar)
        val abdt = ActionBarDrawerToggle(
            this,
            binding.drawerLayout,
            binding.toolbar,
            R.string.openDrawer,
            R.string.closeDrawer
        )
        binding.drawerLayout.addDrawerListener(abdt)
        abdt.syncState()

        this.presenter = MainPresenter(this)

        this.connMgr = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        this.progressOverlay = findViewById(R.id.progress_overlay)

        this.fragmentHome = HomeFragment.newInstance(this.presenter)
        this.fragmentSearch = SearchFragment.newInstance(this.presenter)
        this.fragmentHistory = HistoryFragment.newInstance(this.presenter)
        this.fragmentArtist = ArtistFragment.newInstance(this.presenter)
        this.fragmentAlbum = AlbumFragment.newInstance(this.presenter)
        this.fragmentSong = SongFragment.newInstance(this.presenter)
        this.fragmentSetting = SettingFragment.newInstance(this.presenter)
        this.fragmentManager = this.supportFragmentManager

        this.changePage("Home")
    }

    override fun changePage(page: String) {
        val ft: FragmentTransaction = this.fragmentManager.beginTransaction()
        when(page) {
            "Home" -> {
                this.fragmentManager.popBackStackImmediate()
                ft.replace(binding.fragmentContainer.id, this.fragmentHome, "Home").addToBackStack(null)
            }
            "Search" -> {
                this.fragmentManager.popBackStackImmediate()
                ft.replace(binding.fragmentContainer.id, this.fragmentSearch, "Search").addToBackStack(null)
            }
            "History" -> {
                this.fragmentManager.popBackStackImmediate()
                ft.replace(binding.fragmentContainer.id, this.fragmentHistory, "History").addToBackStack(null)
            }
            "Setting" -> {
                this.fragmentManager.popBackStackImmediate()
                ft.replace(binding.fragmentContainer.id, this.fragmentSetting, "Setting").addToBackStack(null)
            }
            "Artist" -> {
                ft.replace(binding.fragmentContainer.id, this.fragmentArtist, "Artist").addToBackStack(null)
            }
            "Album" -> {
                ft.replace(binding.fragmentContainer.id, this.fragmentAlbum, "Album").addToBackStack(null)
            }
            "Song" -> {
                ft.replace(binding.fragmentContainer.id, this.fragmentSong, "Song").addToBackStack(null)
            }
        }
        binding.drawerLayout.closeDrawers()
        ft.commit()
    }

    override fun closeApplication() {
        this.moveTaskToBack(true)
        this.finish()
    }

    override fun closeKeyboard(openKey: Boolean) {
        if(!openKey) {
            val view: View? = this.currentFocus
            if (view != null) {
                val imm: InputMethodManager =
                        this.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(view.windowToken, 0)
            }
        }
    }

    override fun checkNetworkConn(): Boolean {
        return this.connMgr.activeNetworkInfo != null
    }

    override fun loadList(category: String) {
        this.fragmentSearch.initializeAdapter(category)
    }

    override fun loadAlbumList() {
        this.fragmentArtist.initializeAdapter()
    }

    override fun loadSongList() {
        this.fragmentAlbum.initializeAdapter()
    }

    override fun setInvisible() {
        progressOverlay.visibility = View.INVISIBLE
    }

    override fun setVisible() {
        progressOverlay.visibility = View.VISIBLE
    }

    override fun reloadFragment(tag: String) {
        val frg = this.fragmentManager.findFragmentByTag(tag)
        val ft: FragmentTransaction = this.fragmentManager.beginTransaction()
        ft.detach(frg!!)
        ft.attach(frg)
        ft.commit()
    }

    override fun reloadActivity() {
        finish()
        startActivity(getIntent())
    }
}