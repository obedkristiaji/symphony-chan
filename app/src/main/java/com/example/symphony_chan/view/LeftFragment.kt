package com.example.symphony_chan.view

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ListView
import androidx.fragment.app.Fragment
import com.example.symphony_chan.adapter.LeftListAdapter
import com.example.symphony_chan.databinding.FragmentLeftBinding
import com.example.symphony_chan.model.Left

class LeftFragment : Fragment() {
    private lateinit var binding: FragmentLeftBinding
    private lateinit var listener: IMainActivity

    init {

    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        binding = FragmentLeftBinding.inflate(inflater, container, false)

        val left: ListView = binding.lstLeft as ListView
        val listLeft = listOf<Left>(
                Left("Home", 1),
                Left("Search", 2),
                Left("History", 3),
                Left("Setting", 4),
                Left("Exit", 5)
        )

        val adapter = LeftListAdapter(activity!!, listLeft, this.listener)
        left.adapter = adapter

        return binding.root
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if(context is IMainActivity) {
            this.listener = context as IMainActivity
        } else {
            throw ClassCastException(context.toString()
                    + " must implement FragmentListener")
        }
    }
}