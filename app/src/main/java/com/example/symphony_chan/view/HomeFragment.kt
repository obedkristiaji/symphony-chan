package com.example.symphony_chan.view

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.symphony_chan.databinding.FragmentHomeBinding
import com.example.symphony_chan.presenter.IMainPresenter

class HomeFragment : Fragment() {
    private lateinit var binding: FragmentHomeBinding
    private lateinit var listener: IMainActivity
    private lateinit var presenter: IMainPresenter

    init {

    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        this.binding = FragmentHomeBinding.inflate(inflater, container, false)

        this.binding.btnSearch.setOnClickListener {
            this.presenter.emptyList()
            this.presenter.loadSearchList(this.binding.etSearch, this.binding.spinnerHome, context!!)
            this.presenter.setText(this.binding.etSearch.text.toString(), this.binding.spinnerHome.selectedItemId)
            this.binding.etSearch.setText("")
            this.presenter.closeKey()
            this.listener.changePage("Search")
        }

        return this.binding.root
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if(context is IMainActivity) {
            this.listener = context as IMainActivity
        } else {
            throw ClassCastException(context.toString()
                    + " must implement FragmentListener")
        }
    }

    override fun onResume() {
        super.onResume()
        this.presenter.darkEdit(this.binding.etSearch)
        this.presenter.darkSpinner(this.binding.spinnerHome)
    }

    companion object {
        fun newInstance(presenter: IMainPresenter): HomeFragment {
            val fragment = HomeFragment()
            fragment.presenter = presenter
            return fragment
        }
    }
}