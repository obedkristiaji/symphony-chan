package com.example.symphony_chan.view

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ListView
import androidx.fragment.app.Fragment
import com.example.symphony_chan.adapter.ArtistListAdapter
import com.example.symphony_chan.adapter.SongListAdapter
import com.example.symphony_chan.databinding.FragmentSearchBinding
import com.example.symphony_chan.presenter.IMainPresenter

class SearchFragment : Fragment() {
    private lateinit var binding: FragmentSearchBinding
    private lateinit var listener: IMainActivity
    private lateinit var presenter: IMainPresenter

    init {

    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        this.binding = FragmentSearchBinding.inflate(inflater, container, false)

        this.binding.etSearch.setText(this.presenter.getText())
        this.binding.spinnerSearch.setSelection((this.presenter.getSpinnerId().toInt()))

        this.binding.btnSearch.setOnClickListener {
            this.presenter.emptyList()
            this.presenter.loadSearchList(this.binding.etSearch, this.binding.spinnerSearch, context!!)
            this.presenter.closeKey()
        }

        return this.binding.root
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if(context is IMainActivity) {
            this.listener = context as IMainActivity
        } else {
            throw ClassCastException(context.toString()
                    + " must implement FragmentListener")
        }
    }

    override fun onResume() {
        super.onResume()
        this.presenter.darkEdit(this.binding.etSearch)
        this.presenter.darkSpinner(this.binding.spinnerSearch)
        this.binding.etSearch.setText(this.presenter.getText())
        this.binding.spinnerSearch.setSelection((this.presenter.getSpinnerId().toInt()))
        var category = if (this.binding.spinnerSearch.getSelectedItem().toString()=="Artist") "artist" else "recording"
        this.initializeAdapter(category)
    }

    companion object {
        fun newInstance(presenter: IMainPresenter): SearchFragment {
            val fragment = SearchFragment()
            fragment.presenter = presenter
            return fragment
        }
    }

    fun initializeAdapter(category: String) {
        val lstSearch: ListView = binding.lstSearch as ListView
        val songAdapter = SongListAdapter(activity!!, this.presenter)
        val artistAdapter = ArtistListAdapter(activity!!, this.presenter)

        if(category == "artist") {
            lstSearch.adapter = artistAdapter
            (lstSearch.adapter as ArtistListAdapter).update(this.presenter.getArtistList())
        } else {
            lstSearch.adapter = songAdapter
            (lstSearch.adapter as SongListAdapter).update(this.presenter.getHistoryList())
        }
        this.presenter.setText(this.binding.etSearch.text.toString(), this.binding.spinnerSearch.selectedItemId)
    }
}