package com.example.symphony_chan.view

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.bumptech.glide.load.model.GlideUrl
import com.bumptech.glide.load.model.LazyHeaders
import com.example.symphony_chan.databinding.FragmentSongBinding
import com.example.symphony_chan.model.Song
import com.example.symphony_chan.presenter.IMainPresenter

class SongFragment : Fragment() {
    private lateinit var binding: FragmentSongBinding
    private lateinit var listener: IMainActivity
    private lateinit var presenter: IMainPresenter
    private lateinit var song: Song

    init {

    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        this.binding = FragmentSongBinding.inflate(inflater, container, false)

        this.song = this.presenter.getSong()

        val circularProgressDrawable = CircularProgressDrawable(context!!)
        circularProgressDrawable.strokeWidth = 5f
        circularProgressDrawable.centerRadius = 30f
        circularProgressDrawable.start()

        val glideUrl = GlideUrl(song.getImage(), LazyHeaders.Builder()
                .addHeader("Accept", "image/jpeg")
                .build()
        )

        GlideApp
            .with(this)
            .load(glideUrl)
            .placeholder(circularProgressDrawable)
            .into(this.binding.ivSong)

        this.initiateSong()

        return this.binding.root
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if(context is IMainActivity) {
            this.listener = context as IMainActivity
        } else {
            throw ClassCastException(context.toString()
                    + " must implement FragmentListener")
        }
    }

    override fun onResume() {
        super.onResume()
    }

    companion object {
        fun newInstance(presenter: IMainPresenter): SongFragment {
            val fragment = SongFragment()
            fragment.presenter = presenter
            return fragment
        }
    }

    private fun initiateSong() {
        this.binding.tvTitle.text = this.song.getTitle()
        this.binding.tvArtist.text = this.song.getArtist()
        this.binding.tvRelease.text = this.song.getRelease()
    }
}