package com.example.symphony_chan.view

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ListView
import androidx.fragment.app.Fragment
import com.example.symphony_chan.R
import com.example.symphony_chan.adapter.AlbumListAdapter
import com.example.symphony_chan.databinding.FragmentArtistBinding
import com.example.symphony_chan.model.Artist
import com.example.symphony_chan.presenter.IMainPresenter

class ArtistFragment : Fragment() {
    private lateinit var binding: FragmentArtistBinding
    private lateinit var listener: IMainActivity
    private lateinit var presenter: IMainPresenter
    private lateinit var artist: Artist

    init {

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        this.binding = FragmentArtistBinding.inflate(inflater, container, false)

        this.artist = this.presenter.getArtist()
        this.initiateArtist()

        return this.binding.root
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if(context is IMainActivity) {
            this.listener = context as IMainActivity
        } else {
            throw ClassCastException(context.toString()
                        + " must implement FragmentListener"
            )
        }
    }

    override fun onResume() {
        super.onResume()
        this.initializeAdapter()
    }

    companion object {
        fun newInstance(presenter: IMainPresenter): ArtistFragment {
            val fragment = ArtistFragment()
            fragment.presenter = presenter
            return fragment
        }
    }

    private fun initiateArtist() {
        this.binding.tvName.text = this.artist.getName()
        this.binding.tvDetail.text = this.artist.getDetail()
        this.binding.tvCountry.text = this.artist.getCountry()
        this.binding.tvBirthday.text = this.artist.getBirth()
    }

    fun initializeAdapter() {
        if(this.presenter.getAlbumList().isEmpty()) {
            this.binding.tvAlbum.setText(R.string.tvNoDiscography)
        } else {
            this.binding.tvAlbum.setText(R.string.tvDiscography)
        }
        val lstSong: ListView = binding.lstSong as ListView
        val albumAdapter = AlbumListAdapter(activity!!, this.presenter)
        lstSong.adapter = albumAdapter
        (lstSong.adapter as AlbumListAdapter).update(this.presenter.getAlbumList())
    }
}