package com.example.symphony_chan.view

interface IMainActivity {
    fun changePage(page: String)
    fun closeApplication()
    fun closeKeyboard(openKey: Boolean)
    fun checkNetworkConn(): Boolean
    fun loadList(category: String)
    fun loadAlbumList()
    fun loadSongList()
    fun setInvisible()
    fun setVisible()
    fun reloadFragment(tag: String)
    fun reloadActivity()
}