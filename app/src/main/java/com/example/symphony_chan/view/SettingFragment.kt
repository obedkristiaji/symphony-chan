package com.example.symphony_chan.view

import android.app.AlertDialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.example.symphony_chan.databinding.FragmentSettingBinding
import com.example.symphony_chan.presenter.IMainPresenter

class SettingFragment : Fragment() {
    private lateinit var binding: FragmentSettingBinding
    private lateinit var listener: IMainActivity
    private lateinit var presenter: IMainPresenter
    private lateinit var builder: AlertDialog.Builder

    init {

    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        this.binding = FragmentSettingBinding.inflate(inflater, container, false)

        this.builder = AlertDialog.Builder(activity)

        this.binding.btnDark.setOnClickListener {
            this.presenter.darkMode()
        }

        this.binding.btnClear.setOnClickListener {
            this.builder.setTitle("Apakah kamu yakin ingin menghapus history pencarian?")
            this.builder.setPositiveButton("Yes") { dialog, which ->
                this.presenter.clearData()
                Toast.makeText(this.activity, "Berhasil dihapus", Toast.LENGTH_SHORT).show()
            }

            this.builder.setNegativeButton("No") { dialog, which ->
                Toast.makeText(this.activity, "Gagal dihapus", Toast.LENGTH_SHORT).show()
            }

            this.builder.show()
        }

        return this.binding.root
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if(context is IMainActivity) {
            this.listener = context as IMainActivity
        } else {
            throw ClassCastException(context.toString()
                    + " must implement FragmentListener")
        }
    }

    override fun onResume() {
        super.onResume()
        this.presenter.darkBtn(this.binding.btnDark)
    }

    companion object {
        fun newInstance(presenter: IMainPresenter): SettingFragment {
            val fragment = SettingFragment()
            fragment.presenter = presenter
            return fragment
        }
    }
}