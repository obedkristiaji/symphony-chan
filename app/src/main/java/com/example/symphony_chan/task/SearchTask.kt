package com.example.symphony_chan.task

import android.content.Context
import android.util.Log
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.example.symphony_chan.model.Artist
import com.example.symphony_chan.model.Song
import com.example.symphony_chan.presenter.IMainPresenter
import com.example.symphony_chan.view.IMainActivity
import org.json.JSONArray
import org.json.JSONObject

class SearchTask(private val presenter: IMainPresenter, private val listener: IMainActivity, private val context: Context) {
    private val baseUrl: String = "http://musicbrainz.org/ws/2/"

    fun execute(query: String, category: String) {
        val jsonInput = JSONObject()
        this.callVolley(jsonInput, query, category)
    }

    private fun callVolley(json: JSONObject, query: String, category: String) {
        val queue: RequestQueue = Volley.newRequestQueue(this.context)
        val request = JsonObjectRequest(
            Request.Method.GET,
            this.baseUrl + category + "?query=" + query + "&fmt=json",
            json,
            ResponseListener(category),
            ErrorListener()
        )

        queue.add(request)
    }

    private fun processResult(json: JSONArray, category: String) {
        if(category == "artist") {
            for(i in 0 until json.length()) {
                var id = if(json.getJSONObject(i).has("id")) json.getJSONObject(i).get("id").toString() else "-"
                var name = if(json.getJSONObject(i).has("name")) json.getJSONObject(i).get("name").toString() else "-"
                var detail = if(json.getJSONObject(i).has("disambiguation")) json.getJSONObject(i).get("disambiguation").toString() else "-"
                var country = "-"
                if(json.getJSONObject(i).has("area")){
                    var countryJSON = json.getJSONObject(i).get("area") as JSONObject
                    country = countryJSON.get("name").toString()
                }
                var birth = "-"
                if(json.getJSONObject(i).has("life-span")){
                    var birthJSON = json.getJSONObject(i).get("life-span") as JSONObject
                    birth = if(birthJSON.has("begin")) birthJSON.get("begin").toString() else "-"
                }
                this.presenter.addArtist(Artist(id, name, detail, country,  birth))
            }
        } else {
            for(i in 0 until json.length()) {
                var id = if(json.getJSONObject(i).has("id")) json.getJSONObject(i).get("id").toString() else "-"
                var albumId = "-"
                if(json.getJSONObject(i).has("releases")) {
                    var releaseArr = json.getJSONObject(i).get("releases") as JSONArray
                    albumId = if (releaseArr.getJSONObject(0).has("id")) releaseArr.getJSONObject(0).get("id").toString() else "-"
                }
                var title = if(json.getJSONObject(i).has("title")) json.getJSONObject(i).get("title").toString() else "-"
                var artist = ""
                if(json.getJSONObject(i).has("artist-credit")) {
                    var artistArr = json.getJSONObject(i).get("artist-credit") as JSONArray
                    for (j in 0 until artistArr.length()) {
                        if(artistArr.getJSONObject(j).has("name")) {
                            if (j == artistArr.length() - 1) {
                                artist += artistArr.getJSONObject(j).get("name").toString()
                            } else {
                                artist += artistArr.getJSONObject(j).get("name").toString() + ", "
                            }
                        } else {
                            artist = "-"
                        }
                    }
                }else {
                    artist = "-"
                }
                var date = if(json.getJSONObject(i).has("first-release-date")) json.getJSONObject(i).get("first-release-date").toString() else "-"
                this.presenter.addSong(Song(id ,albumId, "https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/eb777e7a-7d3c-487e-865a-fc83920564a1/d7kpm65-437b2b46-06cd-4a86-9041-cc8c3737c6f0.jpg?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOiIsImlzcyI6InVybjphcHA6Iiwib2JqIjpbW3sicGF0aCI6IlwvZlwvZWI3NzdlN2EtN2QzYy00ODdlLTg2NWEtZmM4MzkyMDU2NGExXC9kN2twbTY1LTQzN2IyYjQ2LTA2Y2QtNGE4Ni05MDQxLWNjOGMzNzM3YzZmMC5qcGcifV1dLCJhdWQiOlsidXJuOnNlcnZpY2U6ZmlsZS5kb3dubG9hZCJdfQ.jgGhagSdo7F0atzSBenaqBDiHFUQiHmQQKDIny5cA2o", title, artist, date))
            }
        }
        this.presenter.setHistoryList(this.presenter.getSongList())
        this.listener.loadList(category)
        this.listener.setInvisible()
    }

    private inner class ResponseListener(private var category: String): Response.Listener<JSONObject> {
        override fun onResponse(response: JSONObject?) {
            if (response != null) {
                Log.d("response", response.toString())
                processResult(response.getJSONArray(category + "s"), category)
            }
        }
    }

    private inner class ErrorListener: Response.ErrorListener {
        override fun onErrorResponse(error: VolleyError?) {
            error?.printStackTrace()
        }
    }
}