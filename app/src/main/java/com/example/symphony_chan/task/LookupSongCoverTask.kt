package com.example.symphony_chan.task

import android.content.Context
import android.util.Log
import com.android.volley.*
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.example.symphony_chan.model.Song
import com.example.symphony_chan.presenter.IMainPresenter
import com.example.symphony_chan.view.IMainActivity
import org.json.JSONArray
import org.json.JSONObject
import java.util.concurrent.TimeoutException

class LookupSongCoverTask(private val presenter: IMainPresenter, private val listener: IMainActivity, private val context: Context) {
    private val baseUrl: String = "http://coverartarchive.org/release/"

    fun execute(song: Song) {
        val jsonInput = JSONObject()
        this.callVolley(jsonInput, song)
    }

    private fun callVolley(json: JSONObject, song: Song) {
        val queue: RequestQueue = Volley.newRequestQueue(this.context)
        val request = JsonObjectRequest(
            Request.Method.GET,
            this.baseUrl + song.getAlbumId(),
            json,
            ResponseListener(song),
            ErrorListener(song)
        )

        queue.add(request)
    }

    private fun processResult(json: JSONArray, song:Song) {
        if(json.getJSONObject(0).has("image")) {
            var url = json.getJSONObject(0).get("image").toString()
            song.setImage(url)
        }
        this.presenter.addSongStorage(song)
        this.listener.changePage("Song")
        this.listener.setInvisible()
    }

    private inner class ResponseListener(private val song:Song): Response.Listener<JSONObject> {
        override fun onResponse(response: JSONObject?) {
            try {
                if (response != null) {
                    Log.d("response", response.toString())
                    processResult(response.getJSONArray("images"), song)
                }
            } catch (e: TimeoutException) {
                e.printStackTrace()
            }
        }
    }

    private inner class ErrorListener(private val song: Song): Response.ErrorListener {
        override fun onErrorResponse(error: VolleyError?) {
            val response: NetworkResponse = error!!.networkResponse
            if (response.statusCode == 404 || response.statusCode == 400) {
                error?.printStackTrace()
                presenter.addSongStorage(song)
                listener.changePage("Song")
                listener.setInvisible()
            }
        }
    }
}