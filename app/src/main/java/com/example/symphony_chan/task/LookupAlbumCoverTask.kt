package com.example.symphony_chan.task

import android.content.Context
import android.util.Log
import com.android.volley.*
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.example.symphony_chan.model.Album
import com.example.symphony_chan.presenter.IMainPresenter
import com.example.symphony_chan.view.IMainActivity
import org.json.JSONArray
import org.json.JSONObject
import java.util.concurrent.TimeoutException

class LookupAlbumCoverTask(private val presenter: IMainPresenter, private val listener: IMainActivity, private val context: Context) {
    private val baseUrl: String = "http://coverartarchive.org/release/"

    fun execute(album: Album) {
        val jsonInput = JSONObject()
        this.callVolley(jsonInput, album)
    }

    private fun callVolley(json: JSONObject, album: Album) {
        val queue: RequestQueue = Volley.newRequestQueue(this.context)
        val request = JsonObjectRequest(
                Request.Method.GET,
                this.baseUrl + album.getId(),
                json,
                ResponseListener(album),
                ErrorListener()
        )

        queue.add(request)
    }

    private fun processResult(json: JSONArray, album: Album) {
        if(json.getJSONObject(0).has("image")) {
            var url = json.getJSONObject(0).get("image").toString()
            album.setImage(url)
        }
        this.presenter.loadSongList(context)
        this.listener.changePage("Album")
        this.listener.setInvisible()
    }

    private inner class ResponseListener(private val album: Album): Response.Listener<JSONObject> {
        override fun onResponse(response: JSONObject?) {
            try {
                if (response != null) {
                    Log.d("response", response.toString())
                    processResult(response.getJSONArray("images"), album)
                }
            } catch (e: TimeoutException) {
                e.printStackTrace()
            }
        }
    }

    private inner class ErrorListener(): Response.ErrorListener {
        override fun onErrorResponse(error: VolleyError?) {
            val response: NetworkResponse = error!!.networkResponse
            if (response.statusCode == 404) {
                error?.printStackTrace()
                presenter.loadSongList(context)
                listener.changePage("Album")
                listener.setInvisible()
            }
        }
    }
}