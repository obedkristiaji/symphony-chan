package com.example.symphony_chan.task

import android.content.Context
import android.util.Log
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.example.symphony_chan.model.Album
import com.example.symphony_chan.model.Song
import com.example.symphony_chan.presenter.IMainPresenter
import com.example.symphony_chan.view.IMainActivity
import org.json.JSONArray
import org.json.JSONObject

class LookupSongTask(private val presenter: IMainPresenter, private val listener: IMainActivity, private val context: Context) {
    private val baseUrl: String = "http://musicbrainz.org/ws/2/recording?release="

    fun execute(album: Album) {
        val jsonInput = JSONObject()
        this.callVolley(jsonInput, album)
    }

    private fun callVolley(json: JSONObject, album:Album) {
        val queue: RequestQueue = Volley.newRequestQueue(this.context)
        val request = JsonObjectRequest(
                Request.Method.GET,
                this.baseUrl + album.getId() + "&fmt=json",
                json,
                ResponseListener(album),
                ErrorListener()
        )

        queue.add(request)
    }

    private fun processResult(json: JSONArray, album: Album) {
        for(i in 0 until json.length()) {
            var id = if(json.getJSONObject(i).has("id")) json.getJSONObject(i).get("id").toString() else "-"
            var title = if(json.getJSONObject(i).has("title")) json.getJSONObject(i).get("title").toString() else "-"
            var date = if(json.getJSONObject(i).has("first-release-date")) json.getJSONObject(i).get("first-release-date").toString() else "-"
            this.presenter.addSong(Song(id, album.getId(), "https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/eb777e7a-7d3c-487e-865a-fc83920564a1/d7kpm65-437b2b46-06cd-4a86-9041-cc8c3737c6f0.jpg?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOiIsImlzcyI6InVybjphcHA6Iiwib2JqIjpbW3sicGF0aCI6IlwvZlwvZWI3NzdlN2EtN2QzYy00ODdlLTg2NWEtZmM4MzkyMDU2NGExXC9kN2twbTY1LTQzN2IyYjQ2LTA2Y2QtNGE4Ni05MDQxLWNjOGMzNzM3YzZmMC5qcGcifV1dLCJhdWQiOlsidXJuOnNlcnZpY2U6ZmlsZS5kb3dubG9hZCJdfQ.jgGhagSdo7F0atzSBenaqBDiHFUQiHmQQKDIny5cA2o", title, album.getArtist(), date))
        }
        this.listener.loadSongList()
        this.listener.setInvisible()
    }

    private inner class ResponseListener(private val album: Album): Response.Listener<JSONObject> {
        override fun onResponse(response: JSONObject?) {
            if (response != null) {
                Log.d("response", response.toString())
                processResult(response.getJSONArray("recordings"), album)
            }
        }
    }

    private inner class ErrorListener: Response.ErrorListener {
        override fun onErrorResponse(error: VolleyError?) {
            error?.printStackTrace()
        }
    }
}