# Description
Repository ini merupakan tugas besar 3 mata kuliah __P3B (Pemrograman pada Perangkat Bergerak)__.

# Link
https://gitlab.com/obedkristiaji/symphony-chan.git

# Contributor
- Obed Kristiaji Sudarman (6181801003)
- Ame Fedora Ignacia Ginting (6181801047)
- Muhamad Ariq Pratama (6181801054)

# Mockup
https://www.figma.com/file/KJRyMbqsEuYW0HZzZsrQYU/Tubes-P3B?node-id=1%3A4
